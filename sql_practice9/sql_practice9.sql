/*
9. SQL Practice 9:- Searching using pattern and wildcards.
*/

select id, company,last_name,first_name,email_address,job_title,business_phone,home_phone,mobile_phone,fax_number,address,
city,state_province,zip_postal_code,country_region,web_page,notes,attachments
from customers where first_name like 'a%';