/*
19. SQL Practice 19:- Filter on Aggregate values (HAVING CLAUSE)
*/

SELECT COUNT(order_id), quantity
FROM order_details
GROUP BY quantity
HAVING COUNT(order_id) > 4;