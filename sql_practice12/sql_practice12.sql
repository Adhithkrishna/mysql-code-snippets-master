/*
12. SQL Practice 12:- Join data from two SELECTS using UNION and UNION ALL.
*/

/* UNION OPERATOR */
SELECT city FROM customers
UNION
SELECT city FROM employees
ORDER BY city;

/* UNION ALL OPERATOR */
SELECT city FROM customers
UNION ALL
SELECT city FROM employees
ORDER BY city;