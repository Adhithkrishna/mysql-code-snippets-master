/*
16. SQL Practice 16:- Show cartersian of two tables( CROSS JOIN)
*/


SELECT customers.last_name, customers.first_name, employees.job_title
FROM customers 
CROSS JOIN employees
WHERE customers.id = employees.id;