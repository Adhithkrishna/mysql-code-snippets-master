/*
18. SQL Practice 18:- Display aggregate values f rom a t able (GROUP BY)
*/

SELECT COUNT(ID), city
FROM employees
GROUP BY city;