/*
11. SQL Practice 11:- CASE statements with SQL.
*/

SELECT order_id,quantity,unit_price,
CASE WHEN unit_price > 30 THEN 'price is greater than 30'
WHEN unit_price = 30 THEN 'price is equal to 30'
ELSE 'price is under 30'
END AS Price_Text
FROM order_details;