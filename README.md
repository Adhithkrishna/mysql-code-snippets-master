**SQL EXERCISE WITH SOLUTION**

1. SQL Practice 1:- Select all columns and all rows of the table

2. SQL Practice 2:- Select only needed columns from table.

3. SQL Practice 3:- Select using a numeric criterion.

4. SQL Practice 4:- Select using a string criterion.

5. SQL Practice 5:- Select using AND and OR.

6. SQL Practice 6:- Sort data using ascending and descending.

7. SQL Practice 7:- Provide user friendly ALIAS for column names.

8. SQL Practice 8:- Display unique records from a table.

9. SQL Practice 9:- Searching using pattern and wildcards.

10. SQL Practice 10:- Create runtime calculated columns.

11. SQL Practice 11:- CASE statements with SQL.

12. SQL Practice 12:- Join data from two SELECTS using UNION and UNION ALL.

13. SQL Practice 13:- Show matching data from two tables.( Inner Join)

14. SQL Practice 14:- Show all records from one table and only matching record from other table. ( Left and Right)

15. SQL Practice 15:- Show all records from matching or unmatching. (FULL outer join).

16. SQL Practice 16:- Show cartersian of two tables( CROSS JOIN)

17. SQL Practice 17:- Writing a complex SQL inner join statement.

18. SQL Practice 18:- Display aggregate values from a table (GROUP BY)

19. SQL Practice 19:- Filter on Aggregate values (HAVING CLAUSE)

20. SQL Practice 20:- Self Join

21. SQL Practice 21:- ISNULL

22. SQL Practice 22:- Sub Queries

23. SQL Practice 23:- Co-related Queries

24. SQL Practice 24:- Find Max, Min and Average.

25. SQL Practice 25:- Find the between numeric values.

26. SQL Practice 26:- Dump table data in to new table (SELECT INTO)

27. SQL Practice 27:- Insert data into table.

28. SQL Practice 28:- Insert bulk data in existing table

29. SQL Practice 29:- Update data into table.

30. SQL Practice 30:- Delete data from a table.

31. SQL Practice 31:- The SQL designer window.
