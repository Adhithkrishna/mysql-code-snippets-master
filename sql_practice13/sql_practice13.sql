/*
13. SQL Practice 13:- Show matching data from two tables.( Inner Join)
*/

SELECT orders.employee_id,orders.customer_id,orders.ship_name,orders.id AS ORDERS_ID
, order_details.id AS  ORDER_DETILS_ID ,order_details.quantity,order_details.unit_price
FROM orders
INNER JOIN order_details ON orders.id = order_details.id;