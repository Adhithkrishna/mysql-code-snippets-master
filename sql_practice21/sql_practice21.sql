
/*
21. SQL Practice 21:- ISNULL
*/

select id as 'ID', employee_id, customer_id,
isnull(shipped_date) 
from orders;
