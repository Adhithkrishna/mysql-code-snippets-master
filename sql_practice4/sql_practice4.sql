/*
4. SQL Practice 4:- Select using a string criterion
*/
SELECT id, last_name, first_name, job_title 
FROM suppliers 
WHERE job_title IN('Sales Manager');