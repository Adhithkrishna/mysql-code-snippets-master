/*
7. SQL Practice 7:- Provide user friendly ALIAS for column names.
*/

select id AS "Sup_ID", company AS "Company_Name", last_name As "LastName", first_name As
"FirstName", email_address AS "E-mail", job_title AS "Designation", business_phone AS "Business_No",
home_phone AS "Landline_No", mobile_phone AS "Mobile_No", fax_number AS "Fax_No",
address As "Address", city AS "City", state_province AS "State", zip_postal_code As "Pincode",
country_region As "Country", web_page As "Website", notes As "Notes", attachments AS
"Attachments" from suppliers ;

