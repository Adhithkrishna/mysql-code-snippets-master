/*
27. SQL Practice 27:- Insert data into table.
*/

INSERT INTO suppliers (id,company,last_name,first_name,email_address,job_title,business_phone,home_phone,
mobile_phone,fax_number,address,city,state_province,zip_postal_code,country_region,web_page,notes,attachments) 
VALUES ('11','Supplier K','Raj','Jithin',NULL,'Sales Representative',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

SELECT id, company, last_name, first_name, email_address, job_title 
FROM suppliers;

