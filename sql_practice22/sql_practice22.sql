/*
22. SQL Practice 22:- Sub Queries
*/


SELECT * 
   FROM products
   WHERE ID IN (SELECT ID 
         FROM products
         WHERE standard_cost> 20) ;