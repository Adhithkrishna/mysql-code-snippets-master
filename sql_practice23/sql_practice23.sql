/*
23. SQL Practice 23:- Co-related Queries
*/


SELECT products.id, products.product_code, products.product_name, products.category
FROM products
WHERE products.category='Beverages';