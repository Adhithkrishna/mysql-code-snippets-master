/*
14. SQL Practice 14:- Show all records from one table and only matching record from other table. ( Left and Right).
*/

SELECT suppliers.company, suppliers.last_name, suppliers.first_name, products.product_name,products.product_code
FROM suppliers
LEFT JOIN products
ON suppliers.id=products.id
ORDER BY suppliers.company;