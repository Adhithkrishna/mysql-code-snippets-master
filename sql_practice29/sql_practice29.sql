/*
29. SQL Practice 29:- Update data into table.
*/

UPDATE purchase_order_details
SET unit_cost='20', date_received='2006-01-20 00:00:00'
WHERE id=238;

SELECT id, purchase_order_id, product_id, quantity, unit_cost, date_received, posted_to_inventory, inventory_id
FROM purchase_order_details;

